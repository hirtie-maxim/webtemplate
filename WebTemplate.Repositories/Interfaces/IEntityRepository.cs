﻿using WebTemplate.Models.Base;
using WebTemplate.Repositories.Base;

namespace WebTemplate.Repositories.Interfaces
{
    public interface IEntityRepository<T> : IGenericRepository<T>
        where T : BaseEntity
    {
         
    }
}