﻿using System.Data.Entity;
using WebTemplate.Models.Base;
using WebTemplate.Repositories.Base;
using WebTemplate.Repositories.Interfaces;

namespace WebTemplate.Repositories.Implementation
{
    public class EntityRepository<T> : GenericRepository<T>, IEntityRepository<T>
        where T : BaseEntity
    {
        public EntityRepository(DbContext context)
            : base(context)
        {
        }
    }
}
