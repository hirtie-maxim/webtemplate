﻿using System.Collections.Generic;
using WebTemplate.Models.Base;

namespace WebTemplate.Services.Base
{
    public interface IEntityService<T> : IService where T : BaseEntity
    {
        void Create(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        void Update(T entity);
    }
}
