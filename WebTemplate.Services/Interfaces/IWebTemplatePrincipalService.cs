﻿using System.Security.Principal;
using WebTemplate.Services.Base;

namespace WebTemplate.Services.Interfaces
{
    public interface IWebTemplatePrincipalService : IService
    {
        IPrincipal GetCurrent();
    }
}