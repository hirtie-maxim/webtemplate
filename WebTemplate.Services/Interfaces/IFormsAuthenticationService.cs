﻿using WebTemplate.Models;
using WebTemplate.Services.Base;

namespace WebTemplate.Services.Interfaces
{
    public interface IFormsAuthenticationService : IService
    {
        void SignIn(User user, bool createPersistentCookie);
        void SignOut();
    }
}