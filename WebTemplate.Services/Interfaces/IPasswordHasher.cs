﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTemplate.DTO;
using WebTemplate.Services.Base;

namespace WebTemplate.Services.Interfaces
{
    public interface IPasswordHasher : IService
    {
        Passphrase Hash(string password);
        bool Equals(string password, string salt, string hash);
        string GenerateRandomSalt(int size);
    }
}
