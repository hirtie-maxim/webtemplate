﻿using WebTemplate.Models.Base;
using WebTemplate.Services.Base;

namespace WebTemplate.Services.Interfaces
{
    public interface IGenericService<T> : IEntityService<T> 
        where T : BaseEntity
    {
         
    }
}