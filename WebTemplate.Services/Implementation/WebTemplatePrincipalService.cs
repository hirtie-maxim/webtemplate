﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using WebTemplate.DTO;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Services.Implementation
{
    public class WebTemplatePrincipalService : IWebTemplatePrincipalService
    {
        private readonly HttpContextBase _context;

        public WebTemplatePrincipalService(HttpContextBase context)
        {
            _context = context;
        }

        #region IPrincipalService Members

        public IPrincipal GetCurrent()
        {
            IPrincipal user = _context.User;
            // if they are already signed in, and conversion has happened
            if (user != null && user is WebTemplatePrincipal)
                return user;

            // if they are signed in, but conversion has still not happened
            if (user != null && user.Identity.IsAuthenticated && user.Identity is FormsIdentity)
            {
                var id = (FormsIdentity)_context.User.Identity;

                var ticket = id.Ticket;
                if (FormsAuthentication.SlidingExpiration)
                    ticket = FormsAuthentication.RenewTicketIfOld(ticket);

                var fid = new WebTemplateIdentity(ticket);
                return new WebTemplatePrincipal(fid);
            }

            // not sure what's happening, let's just default here to a Guest
            return new WebTemplatePrincipal(new WebTemplateIdentity((FormsAuthenticationTicket)null));
        }

        #endregion
    }
}
