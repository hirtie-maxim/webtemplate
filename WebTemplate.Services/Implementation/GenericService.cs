﻿using System.Collections.Generic;
using WebTemplate.DAL;
using WebTemplate.Models.Base;
using WebTemplate.Repositories;
using WebTemplate.Repositories.Interfaces;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Services.Implementation
{
    public class GenericService<T> : IGenericService<T>
        where T : BaseEntity
    {
        private readonly IEntityRepository<T> _repository;

        public GenericService(IEntityRepository<T> repository)
        {
            this._repository = repository;
        }

        public void Create(T entity)
        {
            this._repository.Add(entity);
            this._repository.Save();
        }

        public void Delete(T entity)
        {
            this._repository.Delete(entity);
            this._repository.Save();
        }

        public IEnumerable<T> GetAll()
        {
            return this._repository.GetAll();
        }

        public void Update(T entity)
        {
            this._repository.Edit(entity);
            this._repository.Save();
        }
    }
}