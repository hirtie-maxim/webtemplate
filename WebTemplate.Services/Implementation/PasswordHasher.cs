﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using WebTemplate.DTO;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Services.Implementation
{
    public class PasswordHasher : IPasswordHasher
    {
        private const int SaltSize = 64;

        public Passphrase Hash(string password)
        {
            if (password == null) throw new ArgumentNullException("password");

            byte[] passwordBytes = Encoding.Unicode.GetBytes(password);
            byte[] saltBytes = CreateRandomSalt();
            string hashedPassword = ComputeHash(passwordBytes, saltBytes);

            return new Passphrase
            {
                Hash = hashedPassword,
                Salt = Convert.ToBase64String(saltBytes)
            };
        }

        public bool Equals(string password, string salt, string hash)
        {
            return String.CompareOrdinal(hash, Hash(password, salt)) == 0;
        }

        public string GenerateRandomSalt(int size = SaltSize)
        {
            return Convert.ToBase64String(CreateRandomSalt(size));
        }

        private string ComputeHash(byte[] password, byte[] salt)
        {
            var passwordAndSalt = new byte[salt.Length + password.Length];

            Buffer.BlockCopy(salt, 0, passwordAndSalt, 0, salt.Length);
            Buffer.BlockCopy(password, 0, passwordAndSalt, salt.Length, password.Length);
            byte[] computedHash;
            using (HashAlgorithm algorithm = new SHA256Managed())
            {
                computedHash = algorithm.ComputeHash(passwordAndSalt);
            }
            return Convert.ToBase64String(computedHash);
        }

        private string Hash(string password, string salt)
        {
            return ComputeHash(Encoding.Unicode.GetBytes(password), Convert.FromBase64String(salt));
        }

        private byte[] CreateRandomSalt(int size = SaltSize)
        {
            if (size <= 0)
                throw new ArgumentException("size must be greater than zero.");

            var saltBytes = new Byte[size];
            using (var rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(saltBytes);
            }
            return saltBytes;
        }
    }
}
