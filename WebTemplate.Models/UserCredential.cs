﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTemplate.Models.Base;

namespace WebTemplate.Models
{
    public class UserCredential : AuditableEntity<int>
    {
        public string Salt { get; set; }
        public string Hash { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public bool RememberMe { get; set; }
    }
}
