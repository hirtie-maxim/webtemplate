﻿using WebTemplate.Models.Base;

namespace WebTemplate.Models
{
    public class User : AuditableEntity<int>
    {
        public string Email { get; set; }
        public string Username { get; set; }

        public virtual UserCredential Credential { get; set; }
    }
}
