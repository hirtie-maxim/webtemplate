﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTemplate.Models.Base;

namespace WebTemplate.Models
{
    public enum Role
    {
        User = 0,
        Admin = 1,
        Guest = 2
    }
}
