﻿using System.ComponentModel.DataAnnotations;

namespace WebTemplate.Models.Base
{
    public abstract class Entity<T> : BaseEntity, IEntity<T>
    {
        public virtual T Id { get; set; }
    }
}
