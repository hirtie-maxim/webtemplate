﻿using System.Collections.Generic;
using WebTemplate.Models;

namespace WebTemplate.DTO
{
    public class WebTemplateCookie
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public bool RememberMe { get; set; }
        public ICollection<Role> Roles { get; set; }
    }
}