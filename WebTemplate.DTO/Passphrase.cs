﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebTemplate.DTO
{
    public class Passphrase
    {
        public string Hash { get; set; }
        public string Salt { get; set; }
    }
}
