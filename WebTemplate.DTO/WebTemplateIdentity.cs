﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Web.Security;
using Newtonsoft.Json;
using WebTemplate.Models;

namespace WebTemplate.DTO
{
    public class WebTemplateIdentity : IIdentity
    {
        public WebTemplateIdentity(FormsAuthenticationTicket ticket)
        {
            if (ticket == null)
            {
                Name = "Guest";
                Roles = new List<Role> { Role.Guest };
                return;
            }

            var data = JsonConvert.DeserializeObject<WebTemplateCookie>(ticket.UserData);

            if (data == null)
            {
                AsGuest();
                return;
            }

            Id = data.Id;
            Name = string.IsNullOrWhiteSpace(data.Username) ? data.Email : data.Username;
            Email = data.Email;
            Roles = data.Roles ?? new List<Role> { Role.User };
            RememberMe = data.RememberMe;
        }

        public WebTemplateIdentity(User user)
        {
            if (user == null)
            {
                AsGuest();
                return;
            }

            Id = user.Id;
            Email = user.Email;
            Name = string.IsNullOrWhiteSpace(user.Username) ? user.Email : user.Username;
            Roles = user.Credential.Roles ?? new List<Role> { Role.User };
        }

        private void AsGuest()
        {
            Name = "Guest";
            Roles = new List<Role> { Role.User };
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool RememberMe { get; set; }
        public ICollection<Role> Roles { get; set; }

        #region IIdentity Members

        public string AuthenticationType
        {
            get { return "MuchoForms"; }
        }

        public bool IsAuthenticated
        {
            get { return !(Id == 0 || string.IsNullOrWhiteSpace(Email)); }
        }

        public string Name { get; protected set; }

        #endregion
    }
}
