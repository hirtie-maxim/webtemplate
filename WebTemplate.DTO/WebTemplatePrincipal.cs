﻿using System;
using System.Linq;
using System.Security.Principal;
using WebTemplate.Models;

namespace WebTemplate.DTO
{
    public  class WebTemplatePrincipal : IPrincipal
    {
        private readonly WebTemplateIdentity _identity;

        public WebTemplatePrincipal(WebTemplateIdentity identity)
        {
            _identity = identity;
        }

        #region IPrincipal Members

        public bool IsInRole(string role)
        {
            Role userRole = default(Role);
            if (Enum.TryParse(role, out userRole))
            {
                return _identity.Roles.Any(current => current == userRole);
            }
            return false;
        }

        public IIdentity Identity
        {
            get { return _identity; }
        }

        public WebTemplateIdentity Information
        {
            get { return _identity; }
        }

        public bool IsUser
        {
            get { return !IsGuest; }
        }

        public bool IsGuest
        {
            get { return IsInRole("guest"); }
        }

        #endregion
    }
}
