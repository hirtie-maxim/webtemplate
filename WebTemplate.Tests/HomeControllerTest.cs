﻿using System.Collections.Generic;
using System.Web.Mvc;
using FakeO;
using Moq;
using NUnit.Framework;
using WebTemplate.Areas.Default.Controllers;
using WebTemplate.Models;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Tests
{
    [TestFixture]
    public class HomeControllerTest
    {
        private Mock<IGenericService<User>> userService;


        [SetUp]
        public void SetUp()
        {
            this.userService = new Mock<IGenericService<User>>();
        }

        [Test]
        public void Index_Has_User_Test()
        {
            //setup
            var username = Internet.UserName();
            this.userService.Setup(s => s.GetAll()).Returns(new List<User>
            {
                new User
                {
                    Username = username
                }
            });

            //act
            var controller = new HomeController(userService.Object);
            ViewResult model = (ViewResult) controller.Index();
            var result = (User) model.Model;

            //assert
            Assert.AreEqual(username, result.Username);
        }
    }
}
