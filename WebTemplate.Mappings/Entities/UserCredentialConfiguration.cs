﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebTemplate.Models;

namespace WebTemplate.Mappings.Entities
{
    public class UserCredentialConfiguration : EntityTypeConfiguration<UserCredential>
    {
        public UserCredentialConfiguration()
        {
            HasKey(uc => uc.Id);
        }
    }
}
