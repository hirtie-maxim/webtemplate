﻿using System.Data.Entity.ModelConfiguration;
using WebTemplate.Models;

namespace WebTemplate.Mappings.Entities
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.ToTable("dbo.Users");
            HasKey(u => u.Id);
            HasRequired(u => u.Credential).WithRequiredPrincipal().WillCascadeOnDelete();
        }
    }
}
