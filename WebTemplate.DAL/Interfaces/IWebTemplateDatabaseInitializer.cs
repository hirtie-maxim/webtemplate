﻿using System.Data.Entity;

namespace WebTemplate.DAL.Interfaces
{
    public interface IWebTemplateDatabaseInitializer<in TContext> 
        : IDatabaseInitializer<TContext> where TContext : DbContext
    {
         
    }
}