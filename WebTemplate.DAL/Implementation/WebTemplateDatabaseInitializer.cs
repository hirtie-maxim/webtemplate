﻿using System.Data.Entity;
using WebTemplate.DAL.Interfaces;
using WebTemplate.Models;

namespace WebTemplate.DAL.Implementation
{
    public class WebTemplateDatabaseInitializer : DropCreateDatabaseIfModelChanges<WebTemplateContext>, IWebTemplateDatabaseInitializer<WebTemplateContext>
    {
        public override void InitializeDatabase(WebTemplateContext context)
        {
            base.InitializeDatabase(context);
            context.Users.Add(new User
            {
                Username = "Me",
                Email = "me@mail.com"
            });

            context.SaveChanges();
        }
    }
}
