﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Threading;
using WebTemplate.DAL.Interfaces;
using WebTemplate.Models;
using WebTemplate.Models.Base;

namespace WebTemplate.DAL.Implementation
{
    public class WebTemplateContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<UserCredential> Credentials { get; set; }

        public WebTemplateContext(IWebTemplateDatabaseInitializer<WebTemplateContext> initializer)
            : base("Name=WebTemplateContext")
        {
            Database.SetInitializer(initializer);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            var addMethod = typeof(ConfigurationRegistrar)
                .GetMethods()
                .Single(m =>
                        m.Name == "Add" &&
                        m.GetGenericArguments().Any(a => a.Name == "TEntityType"));

            var assemblies = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(a => a.GetName().Name.Contains("WebTemplate"));

            foreach (var assembly in assemblies)
            {
                var configTypes = assembly
                    .GetTypes()
                    .Where(t => t.BaseType != null &&
                                t.BaseType.IsGenericType &&
                                t.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>));

                foreach (var type in configTypes)
                {
                    var entityType = type.BaseType.GetGenericArguments().Single();

                    var entityConfig = assembly.CreateInstance(type.FullName);
                    addMethod.MakeGenericMethod(entityType)
                        .Invoke(modelBuilder.Configurations, new[] { entityConfig });
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity
                    && (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entry in modifiedEntries)
            {
                IAuditableEntity entity = entry.Entity as IAuditableEntity;
                if (entity != null)
                {
                    string identityName = Thread.CurrentPrincipal.Identity.Name;
                    DateTime now = DateTime.UtcNow;

                    if (entry.State == EntityState.Added)
                    {
                        entity.CreatedBy = identityName;
                        entity.CreatedDate = now;
                    }
                    else
                    {
                        base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                        base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                    }

                    entity.UpdatedBy = identityName;
                    entity.UpdatedDate = now;
                }
            }

            return base.SaveChanges();
        }
    }
}
