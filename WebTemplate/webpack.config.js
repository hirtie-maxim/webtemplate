var path = require('path'),
    webpack = require('webpack'),
    AngularPlugin = require('angular-webpack-plugin');

module.exports = {
    entry: "./assets/ts/main.ts",
    output: {
        path: path.join(__dirname, '/scripts'),
        filename: "[name].bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js', '.html'],
        root: [path.join(__dirname, "assets/vendor/bower_components")],
        modulesDirectories: ["bower_components", "node_modules"],
        alias: {
            'angular': "angular/angular.js",
            'ui.bootstrap$': 'angular-bootstrap/ui-bootstrap-tpls.js',
            'ui.router.stateHelper$': 'angular-ui-router.stateHelper/statehelper.js',
            'ui.router': 'angular-ui-router'
        }
    },
    devtool: 'source-map',
    module: {
        loaders: [
            { test: /[\/\\]ui-bootstrap-tpls\.js$/, loader: "ng-loader?ui.bootstrap" },
            { test: /[\/]angular\.js$/, loader: "exports?angular" },
            {
                test: /\.html$/,
                loader: "ngtemplate?relativeTo=assets/templates/&module=templates&prefix=templates" + "/!html"
            },
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader?emitRequireType=false'
            },
            {
                test: /\.scss$/,
                loader: "style!css!sass?outputStyle=expanded&" + "includePaths[]=" + (path.resolve(__dirname, "./assets/vendor/bower_components"))
            },
            { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
            { test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&minetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&minetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&minetype=image/svg+xml" }
        ],
        noParse: [
            /[\/\\]ui-bootstrap-tpls\.js$/
        ]
    },
    plugins: [
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
        ),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new AngularPlugin()
    ]
};