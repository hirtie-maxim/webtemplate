﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebTemplate.Controllers;
using WebTemplate.DTO;
using WebTemplate.Models;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Areas.Default.Controllers
{
    public class UserController : BaseController
    {
        private readonly IPasswordHasher _passwordHasher;
        private readonly IGenericService<User> _userService;

        public UserController(IGenericService<User> userService, IPasswordHasher passwordHasher)
        {
            this._userService = userService;
            this._passwordHasher = passwordHasher;
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                var passphrase = _passwordHasher.Hash(registerModel.Password);
                var user = new User
                {
                    Username = registerModel.Username,
                    Email = registerModel.Email,
                    Credential = new UserCredential
                    {
                        Hash = passphrase.Hash,
                        Salt = passphrase.Salt,
                        RememberMe = false,
                        Roles = new List<Role> { Role.User }
                    }
                };
                this._userService.Create(user);
                return RedirectToAction("Index", "Home");
            }
            return View(registerModel);
        }
    }
}