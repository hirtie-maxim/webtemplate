﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using WebTemplate.Controllers;
using WebTemplate.DTO;
using WebTemplate.Models;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Areas.Default.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IGenericService<User> _userService;
        private readonly IFormsAuthenticationService _formsAuthService;
        private readonly IPasswordHasher _passwordHasher;

        public AccountController(IGenericService<User> userService, IFormsAuthenticationService formsAuthService, IPasswordHasher passwordHasher)
        {
            this._userService = userService;
            this._formsAuthService = formsAuthService;
            this._passwordHasher = passwordHasher;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl)
        {

            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrWhiteSpace(returnUrl))
                    return Redirect(returnUrl);

                return RedirectToAction("Index", "Home");
            }

            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel input)
        {
            var user = _userService.GetAll().FirstOrDefault(u => u.Email == input.Email);
            if (_passwordHasher.Equals(input.Password, user.Credential.Salt, user.Credential.Hash))
            {
                // set cookie
                _formsAuthService.SignIn(user, input.RememberMe);
                return RedirectToAction("Index", "Home");
            }

            return View("Login", input);
        }

        public ActionResult Logout()
        {
            _formsAuthService.SignOut();
            return RedirectToAction("Authenticate");
        }

        public ActionResult Authenticate()
        {
            return View();
        }
    }
}