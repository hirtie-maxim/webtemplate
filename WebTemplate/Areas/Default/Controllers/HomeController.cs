﻿using System.Linq;
using System.Web.Mvc;
using WebTemplate.Controllers;
using WebTemplate.Models;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Areas.Default.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IGenericService<User> _userService;

        public HomeController(IGenericService<User> userService)
        {
            this._userService = userService;
        }

        public ActionResult Index()
        {
            var user = _userService.GetAll().FirstOrDefault();
            return View(user);
        }

        public ActionResult NavigationBar()
        {
            return View();
        }
    }
}