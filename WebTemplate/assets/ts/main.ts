﻿/// <reference path="../typings/tsd.d.ts"/>
/// <reference path="authentication/AuthenticationController.ts"/>
/// <reference path="Application.ts"/>

import Application = require("./Application");
import AuthenticationController = require("./authentication/AuthenticationController");

require("jquery");
require("bootstrap-sass-official");
require("../sass/main.scss");

module WebTemplate {
    "use strict";

    $(() => {
        angular.module('templates', []);
        var webTemplate = angular.module("web-template", ["templates", "ui.bootstrap", "ui.router", "ui.router.stateHelper"]);
        webTemplate.controller("Application", Application);
        webTemplate.controller("AuthenticationController", AuthenticationController);

        webTemplate.config(($stateProvider, $urlRouterProvider, stateHelperProvider) => {
            $urlRouterProvider.otherwise("/");
            // Now set up the states
            stateHelperProvider.setNestedState({
                name: 'home',
                'abstract': true,
                children: [
                    {
                        name: 'auth',
                        'abstract': true,
                        url: '/auth',
                        children: [
                            {
                                name: 'login',
                                url: '#login',
                                onEnter: [
                                    '$stateParams', '$state', '$modal', ($stateParams, $state, $modal) => {
                                        $modal.open({
                                            templateUrl: "Account/Authenticate",
                                            controller: [
                                                '$scope', function($scope) {

                                                }
                                            ]
                                        }).result.finally(function() {
                                            
                                        });
                                    }
                                ]
                            },
                            {
                                name: 'signup',
                                url: '#signup',
                                onEnter: [
                                    '$stateParams', '$state', '$modal', ($stateParams, $state, $modal) => {
                                        $modal.open({
                                            templateUrl: "Account/Authenticate",
                                            controller: [
                                                '$scope', function ($scope) {

                                                }
                                            ]
                                        }).result.finally(function () {

                                        });
                                    }
                                ]
                            }
                        ]
                    }
                ]
            });
        });

        angular.bootstrap($('body')[0], ["web-template"]);
    });
}