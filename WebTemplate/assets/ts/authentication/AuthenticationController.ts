﻿/// <reference path="../../typings/tsd.d.ts"/>

class AuthenticationController {
    public static $inject = [
        "$scope"
    ];

    constructor(public $scope: ng.IScope, private $modal: any) {
        
    }
}

export = AuthenticationController;