﻿var gulp = require("gulp"),
    path = require("path"),
    webpack = require("gulp-webpack-build");

gulp.task("webpack", function() {
    return gulp.src(path.join(webpack.config.CONFIG_FILENAME), { base: path.resolve("./") })
        .pipe(webpack.run())
        .pipe(webpack.format({
            version: false,
            timings: true
        }))
        .pipe(webpack.failAfter({
            errors: true,
            warnings: true
        }));
});