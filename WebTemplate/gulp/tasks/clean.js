﻿var gulp = require("gulp"),
    clean = require("gulp-clean");

gulp.task("clean", function() {
    return gulp.src("scripts/*")
        .pipe(clean({ force: true }));
});