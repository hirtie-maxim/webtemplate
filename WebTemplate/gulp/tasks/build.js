﻿var gulp = require("gulp");

gulp.task("build", ["clean", "webpack"]);
gulp.task("dev-build", ["build", "watch"]);