﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Ninject;
using Ninject.Web.Common;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Infrastructure
{
    public class MvcNinjectApplication : NinjectHttpApplication
    {
        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            DependencyResolver.SetResolver(new LocalNinjectDependencyResolver(kernel));
            return kernel;
        }

        private new void Application_Start()
        {
            base.Application_Start();
        }

        public void Application_AuthenticateRequest()
        {
            var principalService = DependencyResolver.Current.GetService<IWebTemplatePrincipalService>();
            var context = DependencyResolver.Current.GetService<HttpContextBase>();
            // Set the HttpContext's User to our IPrincipal
            context.User = principalService.GetCurrent();
        }
    }
}
