﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Dependencies;
using Ninject;
using WebTemplate.DAL;
using WebTemplate.DAL.Implementation;
using WebTemplate.DAL.Interfaces;
using WebTemplate.DTO;
using WebTemplate.Repositories;
using WebTemplate.Repositories.Implementation;
using WebTemplate.Repositories.Interfaces;
using WebTemplate.Services.Base;
using WebTemplate.Services.Implementation;
using WebTemplate.Services.Interfaces;

namespace WebTemplate.Infrastructure
{
    public class LocalNinjectDependencyResolver : System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel _kernel;

        public LocalNinjectDependencyResolver(IKernel kernel)
        {
            _kernel = kernel;
            AddBindings();
        }

        private void AddBindings()
        {
            this._kernel.Bind<DbContext>().To<WebTemplateContext>();
            this._kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            this._kernel.Bind(typeof (IWebTemplateDatabaseInitializer<>)).To<WebTemplateDatabaseInitializer>();
            this._kernel.Bind(typeof (IGenericService<>)).To(typeof (GenericService<>));
            this._kernel.Bind(typeof (IEntityRepository<>)).To(typeof (EntityRepository<>));
            this._kernel.Bind<IPasswordHasher>().To<PasswordHasher>();
            this._kernel.Bind<IFormsAuthenticationService>().To<FormsAuthenticationService>();
            this._kernel.Bind<IWebTemplatePrincipalService>().To<WebTemplatePrincipalService>();
            this._kernel.Bind<IPrincipal>().To<WebTemplatePrincipal>();
            this._kernel.Bind<IIdentity>().To<WebTemplateIdentity>();
            this._kernel.Bind<HttpContext>().ToMethod(ctx => HttpContext.Current).InTransientScope();
            this._kernel.Bind<HttpContextBase>().ToMethod(ctx => new HttpContextWrapper(HttpContext.Current)).InTransientScope();
        }

        public System.Web.Mvc.IDependencyResolver BeginScope()
        {
            return this;
        }

        public object GetService(Type serviceType)
        {
            return _kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return _kernel.GetAll(serviceType);
            }
            catch (Exception)
            {
                return new List<object>();
            }
        }

        public void Dispose()
        {
        }
    }
}
